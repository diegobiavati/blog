---
title: "Termos famosos da Arma de Engenharia"
date: 2023-05-11T07:46:00-03:00
categories:
  - Exército
  - Arma de Engenharia
  - História
  - História Militar
tags:
  - engenharia
  - exército
  - arma
  - história
---
## Xingu ##
### Etimologia ###
A etimologia da palavra *XINGU* até hoje é desconhecida, estudiosos acreditam que  a tradução seria ”Casa dos Deuses”, sem a certeza de qual seria sua verdadeira raiz subjacente. *XINGU* significa "água boa, água limpa" na língua Kamayurá.

### História ###
"Dia 12 de maio de 2023 recebi a mensagem abaixo transcrita: 

>[11:24, 12/05/2023] +55 35 ???-8117
>
>"Cel bom dia, Sgt Biavati, Sgte do C Eng ESA. O Cel Luciano Rocha Silveira que passou o contato do Sr.
>
>Estou realizando uma pequena pesquisa, de cunho pessoal.
>
>Sr teria alguma informação quanto ao uso do termo *XINGU* na nossa Arma? Não estou encontrando nada muito convincente." 
{:.notice--success}

De momento não soube responder. Pedi um tempo para eu consultar companheiros de turma para que ratificasse e ou retificasse o que eu estava pensando.

Consultei outros grupos de WhatsApp que hospedam oficiais de Engenharia, bem mais antigos, mas as datas não coincidiam. 

Alertado pelo então Cadete Gilberto Machado da Rosa, de que em 1970 já usávamos o brado *XINGU*, na Academia Militar das Agulhas Negras. A conversa foi produtiva pois o cérebro começou produzir.

O brado *XINGU* na Arma de Engenharia é, e foi, bastante usado nas Escolas Militares: AMAN, ESA, CPOR E NPOR. Na tropa por existir outros brados, até regionais, o brado não é tão intensamente usado.

Como disse o então Cadete Albérico Conceição Andrade, outro consultado:

>No início da década de 70 estava no auge a construção de rodovias na Amazônia nessa ocasião os batalhões de engenharia estavam engajados nesse movimento rodoviário.
{:.notice--success}

A ambição mundial sobre a Amazônia sempre foi patente. O primeiro passo foi estabelecer onde começava a área. Para isso, foi estabelecida a Amazônia Legal pela Lei nº 5.173, de 27 de outubro de 1966.

Era lema do governo: Integrar para não Entregar. Para isso, havia necessidade de infraestrutura. E, como sempre, na história de qualquer nação, o Exército foi convocado, para fazê-la. A primeira das infraestrutura teria que ser a rodovia. Os rios são sempre dispersantes em suas direções. Então o 50 BEC foi instalado, em 1966, em Porto Velho sendo o Pioneiro. Posteriormente, ao longo de 1970, foram instalados o 60 em Boa Vista, Roraima, o 70 em Cruzeiro do Sul, Acre, o 80 em Santarém, Pará, e o 90 em Cuiabá, Mato Grosso. A Arma de Engenharia de Engenharia, na AMAN, desafiava a nova geração de oficiais de engenharia para, no contexto da desconhecida Amazônia, participar da aventura de abrir estradas pioneiras.

Iniciativa também do governo, da época, foi criado o Projeto Rondon com a finalidade de mostrar, aos estudantes universitários brasileiros, um outro Brasil que precisaria deles após formados.

Num desses projetos, o PR5, participaram dois futuros oficiais de engenharia, ainda no Segundo Ano da AMAN. Dou a palavra a um deles - Cadete Alberico:

> “De 1969 para 70 nas férias um grupo de Cadetes participou do projeto Rondon. Porto Velho era um polo de distribuição das equipes principalmente para o Acre e para o Amazonas. Os participantes do Projeto Rondon ficaram alojados na Estrada de Ferro Madeira-Mamoré e faziam as refeições no 5º BEC enquanto ficavam aguardando o transporte para as diversas cidades do interior. O pessoal do projeto assistia pela manhã a empolgante formatura do Quinto e alguns Cadetes da Turma de Engenharia, aspirante em1970. Comigo, estava o Cadete Pamplano. No início do ano de 1970, quando retornamos para AMAN, e escolhemos a Arma de Engenharia, nós resolvemos divulgar a canção do Quinto BEC e me parece que aí foi que surgiu o brado de Xingu em vez de Urra. Homenageando desta forma o trabalho da engenharia de construção na Amazônia. O Exército, na época, tinha seu lema: O EXÉRCITO CONSTRÓI”.
{:.notice--success}

Contribuindo com o então Cadete **Albérico**, reforçada pela consulta feita ao então Cadete Pamplano, realmente, no nosso Terceiro ano de AMAN, recém optantes pela Arma de Engenharia, o Cadete **Pamplano** trouxe um gravador de fita, com fita de rolo, comprado na Zona Franca de Manaus, e, nos intervalos do almoço no Cassino do Curso, colocava a rodar a Canção do 5º BEC, canção vibrante dos *Panzerlied* e letra do então Capitão **Pastor**. Distribuiu-se a letra e todos aprendemos, a Canção do 5º BEC. Passamos a cantá-la nos deslocamentos em forma para o Parque de Engenharia onde tínhamos as instruções especificas. O final de canção terminava em HURRA!!!  O hurra foi substituído pelo **XINGU!!!**

Mas de quem foi a ideia do uso do brado e de onde apareceu o XINGU? O uso foi espontâneo. Alguém começou e outros acompanharam. A palavra **XINGU** veio de brincadeira dos cadetes de outras armas. Os Batalhões de Construção eram uma incógnita. A Amazônia, um mistério. Da Amazonia, o que aparecia, algumas vezes, era o trabalho dos irmãos Vilas Boas e eles divulgaram as tribos do alto Rio Xingu, em particular os Xavantes.

Então os cadetes das outras Armas, na brincadeira diziam:

>“o pessoal da engenharia vai tudo para o Xingu...”

>“olha o pessoal do Xingu...”

>“Xingu neles”

Essas brincadeiras foram confirmadas pelo então Cadete Ávila: 

>“Já no quarto ano, estava eu e o cadete Barreto na piscina, desafiamos os de artilharia a saltar do trampolim de dez metros de altura. Não foram e disseram que era coisa pra Xingu. Eu e o Barreto saltamos dando o brado XINGU ao sair da plataforma”.
{:.notice--success}

E usando a palavra da moda, “ressignificamos” o que seria o medo da floresta, da mata, da selva para uma palavra forte, um brado, um grito de guerra.

Finalizando e respondendo a indagação do Sargento Biavati:

>O Brado *XINGU* nasceu da adaptação do final da Canção do 5º BEC, feita pela turma de engenharia, aspirantes de 1971, cantada em 1970 e 1971, cuja canção foi levada à AMAN, pelo então Cadete **Pamplano**, por ocasião de participação de cadetes no Projeto Rondon, em que a palavra *XINGU* viera de brincadeiras dos cadetes de outras armas, pois, diziam que os oficiais de engenharia iriam todos para o Xingu, trabalhar em rodovias.
{:.notice--primary}

### Autor ###
- **Higino** - Cel Eng/71

### Créditos ###
- Cadete Gilberto Machado da Rosa – Cel Eng **Gilberto**
- Cadete Alberico Conceição de Andrade – Cel Eng **Albérico**
- Cadete José Alencar de Ávila – Cel QEM/FC **Ávila**
- Cadete Antônio Fernando de Sá Muniz **Barreto** _(in memoriam)_
- Cadete Orlando Gonçalves Pamplano – Gen Bda **Pamplano**.
- 1º Sargento Diego Eduardo Ferreira Biavati – 1º Sgt Eng **Biavati**, Sgte CEng/ESA"

## Biriba da Engenharia ##
### Criação ###
Criado em 1952, pelo então tenente Ney de Oliveira **Aquino**[^1], quando era instrutor do Curso de Engenharia da Academia Militar das Agulhas Negras. Sua arte foi pintada na parede da ala da Engenharia da AMAN.

[^1]: O tenente Aquino chegou ao posto de coronel, foi o primeiro Subcomandante do 5º Batalhão de Engenharia de Construção (BEC) e também o primeiro Comandante do 6º BEC, não só foi o idealizador e desenhista do Biriba, como também foi o designer dos logotipos dos 5º, 6º, 7º, 8º e 9º BEC e o do 2º Grupamento de Engenharia. O Cel Aquino, quando instrutor da AMAN, também criou o Psilito, ícone da Infantaria

### Significado ###
#### Etimologia ####
 Biriba tem a etimologia um tanto difícil. O significado, em dicionário, é bem diversificado. Pode ser “pedaço de pau como porrete, cacete;” também pode ser “aquele que não tem trato social; caipira;” e até “variedade de jogo, derivado da canastra”. A etimologia, segundo Antenor Nascimento, vem do tupi: *ïmbï'ra* - madeira, pau, árvore e i - pequeno, pouco.

A definição que mais interessa é o significado ligado à arma de engenharia: **biriba - tropeiro de muar**.

Nos trabalhos de ferrovias e rodovias ou no preparo de margens de rios para transposições em teatro de operações de guerra ou de paz, até os anos 60, os transportes de material de aterros e cortes eram executados em carroças puxadas por muares. Em função disso tinham os responsáveis pelos muares tanto para trato: água, ração, descanso; como pelo emprego: ciclo de viagem, carga e descarga. Assim, o biriba era um ente polivalente que tanto entendia dos cuidados com os muares como também da técnica e da organização do canteiro de trabalho. Portanto, o biriba é o “tropeiro de muar”

#### Simbologia ####
Em que pese ter passado o tempo, evoluído a tecnologia e o advir das máquinas de motores potentes o biriba permaneceu na Arma de Engenharia como Símbolo.

Simbolicamente está presente em todas as frentes de trabalho em qualquer que seja a atividade de combate ou de construção. Há sempre o polivalente que entende de tudo, sabe onde está tudo e onde conseguir tudo. Se não tem, cria, adapta, improvisa. Então, o Biriba é um ente invisível em especialidade, mas real no cumprimento da missão que está sempre disposto e nada reclama em face das adversidades que lhes são impostas. Na Engenharia é o sapador mineiro, ou o pontoneiro, ou o trecheiro sempre pronto para o serviço. Nascido na serra gaúcha, ou natural de São Paulo, ou caipira de Minas, ou de qualquer rincão do país, o Biriba pega pesado sem pestanejar com o grito em bom tom: “AO BRAÇO FIRME!” ou “AVANTE REMAR!”

### Autor ###
- Luciano Rocha Silveira – Cel Eng Ref