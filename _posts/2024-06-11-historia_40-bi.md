---
title: "Histórico do 40º Batalhão de Infantaria"
date: 2024-06-11T09:33:00-03:00
categories:
  - Exército
  - Arma de Infantaria
  - História
  - História Militar
tags:
  - infantaria
  - exército
  - arma
  - história
---
>“Guardião das Tradições do Brigadeiro Antônio de Sampaio – Patrono da Arma de Infantaria”

## Criação ##
O 40º Batalhão de Infantaria (BI) teve sua criação em 7 de janeiro de 1890, através da portaria que criou o 36º BI, tendo como sede a cidade de Manaus-AM. Anos mais tarde, passou a se chamar 46º Batalhão de Caçadores (BC). Em 1935, mudou-se de sede, indo para a cidade de Fortaleza, capital do Ceará, onde recebeu a denominação de 23º BC.

## 4º BEC ##
Alguns anos mais tarde, em 1958, o Exército Brasileiro criou o 4º Batalhão de Engenharia de Construção (BEC), em Crateús-CE, com o objetivo de construir uma malha rodoferroviária e de apoiar as obras contra as secas do interior do Estado.

## 2ª Cia Fz ##
Após quinze anos de intensas atividades, as suas instalações foram ocupadas pela 2ª Companhia de Fuzileiros (Cia Fuz) do 23º BC, sendo o dia 08 de março de 1973 a chegada do destacamento precursor, simbolizando a data de origem do Batalhão.

Em 31 de março de 1973, o efetivo da 2ª Cia Fuz completou seu deslocamento para Crateús, recebendo oficialmente o aquartelamento, os cargos e encargos, dando início a sua missão constitucional, mas ainda vinculada ao 23º BC.

Como resultado, em 23 de março de 1973, a portaria nº 40 - DGEF concedeu a autonomia administrativa à 2ª Cia Fuz, a contar de 1º de julho de 1973, tendo esse ato configurado a existência da nova Unidade.

## 40º BI ##

Finalmente, com a Portaria Ministerial nº 97 – Reservada, de 31 de dezembro de 1974, a 2ª Cia Fuz foi transformada em 40º BI, atual denominação, sendo considerada uma Unidade de Guarnição Especial de 2ª Categoria, conforme definido na Portaria Ministerial nº 1784, de 28 de novembro de 1973.

Fonte: [Site do 40º BI](https://40bi.eb.mil.br/index.php/historico)