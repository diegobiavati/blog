---
title: "Escola de Sargentos das Armas (ESA)"
date: 2024-09-19T10:12:00-03:00
categories:
  - Exército
  - Organizações Militares
  - História
  - História Militar
  - ESA
tags:
  - exército
  - história
  - esa
---
A Escola de Sargentos das Armas (ESA) é um estabelecimento de ensino superior (nível tecnólogo) do Exército Brasileiro. Sua sede está situada na cidade de Três Corações no estado de Minas Gerais. É responsável pela formação de sargentos combatentes de carreira das Armas de infantaria, cavalaria, artilharia, engenharia e comunicações do Exército.

Para esse fim seleciona, anualmente, jovens de todas as partes do Brasil, mediante concurso público, oferecendo-lhes ensino militar destinado a aprimorar-lhes o caráter e desenvolver-lhes a capacidade física, também proporcionar, aos futuros sargentos - elos entre o Comando das Organizações Militares e a tropa - sólido embasamento militar.

O seu aquartelamento está sediado às margens do rio Verde. A estrutura é composta por alojamentos, refeitórios, salas de aula, laboratório, espaço cultural, biblioteca, auditório, posto médico, capelania militar, parque de pontes e uma extensa área desportiva constituída por ginásios, campo de futebol, pista de atletismo, piscina, campo de polo, pista hípica e pista de corda. Possui dois campos de instrução: o do Atalaia, com área de 4,6 km² e o General Moacyr Araújo Lopes, CIGMAL[nota 1], este com área de 20 km², distante cerca de 40 km de Três Corações.

## História ##
### Escola de Sargentos (ES) ###
A formação do sargento combatente do Exército Brasileiro teve sua origem, como curso sistematizado, teve sua origem na Ordem do Dia Nº 552, de 28 de maio de 1894, por meio do Decreto Nº 1.199 de 31 de dezembro de 1892, do Vice-Presidente da República dos Estados Unidos do Brasil que aprovou o Regulamento para a Escola de Sargentos. Este documento fez referência às Armas de Infantaria, Cavalaria, Artilharia e Engenharia.

Segundo consta no seu regulamento, o Curso funcionou na Fortaleza de São João, no bairro da Urca, na cidade do Rio de Janeiro, antiga Escola de Aprendizes Artilheiros.

Teve como seu primeiro comandante o Coronel de Artilharia Hermes Rodrigues da Fonseca, sendo extinta em 17 de Dezembro de 1897.

### Escola de Sargentos de Infantaria (ESI) ###

Retornou a funcionar novamente após aprovação do Regulamento da Escola de Sargentos de Infantaria - ESI, no dia 10 de janeiro de 1920, pelo presidente Epitácio Pessoa.

### Escola de Sargentos das Armas (ESA) ###

A ESA foi criada no dia 21 de agosto de 1945, ao término da Segunda Guerra Mundial, por meio do Decreto Nº 7.888, originária da Escola de Sargentos de Infantaria - ESI, ofereceu os cursos de formação de sargentos nas Armas de Infantaria, Cavalaria, Artilharia e de Engenharia. Ocupou, inicialmente, parte das instalações da extinta Escola Militar do Realengo, no Rio de Janeiro-RJ . A primeira turma graduou-se em 1946. Quatro anos mais tarde, foi transferida para Três Corações-MG. Comandava a Escola o Tenente Coronel de Infantaria Miguel Lage Sayão, seu segundo comandante.

No ano de 2017, por meio da Portaria nº 194, de 9 de maio de 2017, o General de Divisão Sérgio da Costa Negraes - Secretário-Geral do Exército, alterou a data de aniversário da ESA de 21 de agosto de 1945 para 28 de maio de 1894, data de criação de seu elemento formador, a "Escola de Sargentos".
