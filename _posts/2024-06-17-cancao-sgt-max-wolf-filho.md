---
title: "Canção Sargento Max Wolf Filho"
date: 2024-06-17T13:22:00-03:00
categories:
  - Exército
  - Canções Militares
  - História
  - História Militar
tags:
  - exército
  - arma
  - história
  - canção
---
## Portaria de Aprovação ##
Portaria – DECEx/C Ex Nº 135 , de 18 de maio de 2022, que aprova e classifica a Obra Musical Militar (OMusMil) para adoção pelo Exército Brasileiro: Canção Sargento Max Wolf Filho.[^1]

## Autores ##
Letra: Capitão Denir Figueiredo\
Música: 1º Tenente Marcos Cesar Toniolo

## YouTube ##
{% include video id="Hy5yZCTzSIs" provider="youtube" %}


## Letra ##
>Em mil novecentos e onze,\
no interior do Paraná,\
nasceu um herói de guerra\
para o Brasil se orgulhar.\
Fiel e audaz voluntário,\
intrépido nas decisões,\
o guerreiro ardoroso não teme,\
cumpre sempre as suas missões.

>No campo ardil da batalha,\
o infante empunha o fuzil,\
carrega no peito estandarte,\
glória do nosso Brasil.\
Deixou para nós o legado\
repleto de fé e ações\
do soldado até o comandante,\
somos todos fiéis guardiões.

>**Estribilho**\
A Pátria enaltece\
seu bravo tão querido,\
sargento Max Wolf Filho,\
valoroso e aguerrido!

## Fontes ##
[Site da Secretaria-Geral do Exército - SGEx](http://www.sgex.eb.mil.br/sg8/006_outras_publicacoes/07_publicacoes_diversas/08_departamento_de_educacao_e_cultura_do_exercito/port_n_135_decex_18maio2022.html)

## Referências ##
[^1]: [Boletim do Exército nº 40/2022](http://www.sgex.eb.mil.br/sistemas/boletim_do_exercito/copiar.php?codarquivo=3517&act=bre)


