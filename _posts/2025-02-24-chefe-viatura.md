---
title: "Chefe de Viatura (Ch Vtr)"
date: 2025-02-24T08:31:00-03:00
categories:
  - Exército
  - Organizações Militares
  - Memento
tags:
  - exército
  - esa
  - memento
---
O artigo apresentado é um compilado de informações sobre as atribuições e obrigatoriedades do Chefe de Viatura (Ch Vtr), não pretendendo esgotar o assunto. Ele visa fornecer um panorama geral das responsabilidades do Ch Vtr, com base em diversas fontes como o Código de Trânsito Brasileiro, o Programa de Instrução Militar 2025 e outras diretrizes e regulamentos militares.

É importante ressaltar que este artigo não substitui a leitura e compreensão completa das fontes originais, que podem conter informações mais detalhadas e específicas sobre o tema.

Recomenda-se que os interessados consultem as fontes originais para obter informações completas e atualizadas sobre as atribuições e obrigatoriedades do Chefe de Viatura.

Em caso de dúvidas, consulte a legislação e as normas internas do Exército Brasileiro (EB).

## Atribuições do Chefe de Viatura (Ch Vtr) ##
1. Orientar o motorista quanto à segurança do deslocamento antes da partida;[^1]
2. Ditar a cadência do deslocamento a todo momento;[^1]
3. Fiscalizar, em todo deslocamento, o permanente cumprimento das normas de segurança e direção defensiva por parte do motorista;[^1]
4. Fiscalizar o se todos os ocupantes estão utilizando este o cinto de segurança ao sair da OM e durante todo deslocamento;[^1]
5. Dispensar os cuidados prescritos quanto às cargas e ao carregamento de viatura;[^2]
6. Deverá conferir se o motorista escalado possui as habilitações e experiência necessárias para o deslocamento previsto, evitando que sejam escalados motoristas;[^3]
em desacordo com as legislações em vigor, bem como a documentação da viatura.
7.  Orientar o  motorista durante a condução da viatura (redução de velocidade, mudança de rota de deslocamento, dentre  outras  conforme situação).[^4]
8. Realizar orientações de caráter geral que direcionam o cumprimento da missão recebida.[^4]

## Obrigatoriedade ##
O deslocamento de viaturas deve, sempre, prever um chefe de viatura ou acompanhante do motorista (caso o motorista seja mais antigo), inclusive quando se tratar de viaturas alugadas (ônibus, van etc.). Esse chefe/acompanhante deve manter-se desperto e atento aos sinais de cansaço do motorista.[^5]

Nos deslocamentos de viaturas operacionais, em comboio ou isoladas, o chefe da viatura, obrigatoriamente um oficial ou graduado mais antigo que o motorista, deve deslocar-se na cabine, ao lado do motorista, e fiscalizar a observância das normas de segurança.[^6]

## Velocidades Máximas ##
 1. A velocidade máxima a ser desenvolvida por viaturas não operacionais, em deslocamento isolado, é determinada pela regulamentação de tráfego civil.[^6]

### Viatura Isolada ### 

| Condição                       | Em Estradas | Em Área Urbana |
|--------------------------------|-------------|----------------|
| Viatura Isolada - Sem reborque | Até 80 km/h | Até 60 km/h    |
| Viatura Isolada - Com reborque | Até 50 km/h | Até 50 km/h    |

* Quando os reboques não dispuserem de freio acionado pela viatura tratora, dos valores da velocidade acima, devem ser abatidos em 10 Km/h.[^6]

### Comboio ### 

| Condição                       | Velocidade Máxima    |
|--------------------------------|----------------------|
| Comboio - Coluna Aberta        | Até 70 km/h          |
| Comboio - Coluna Cerrada       | Até 60 km/h          |
| Comboio - Por infiltração      | Como viatura isolada |

* O Regulador de Marcha de um comboio, deslocando-se na primeira viatura da primeira unidade de marcha, controla a velocidade do deslocamento de todas as viaturas do comboio.

## Legislação de Apoio ##
* [Código de Trânsito Brasileiro](https://www.planalto.gov.br/ccivil_03/leis/l9503compilado.htm)

## Referências ##
[^1]: [Programa de Instrução Militar 2025](https://portaldopreparo.eb.mil.br/ava/pluginfile.php/106988/mod_resource/content/5/01%20-%20%20PIM%202025%20-%20COMPLETO%2019%20Dez%2024.pdf)
[^2]: [Inciso Iv do Art 104 do Regulamento Interno dos Serviços Gerais - RISG](http://www.sgex.eb.mil.br/sg8/001_estatuto_regulamentos_regimentos/02_regulamentos/port_n_816_cmdo_eb_19dez2003.html)
[^3]: [Diretriz Sobre Emprego e Deslocamento de Viaturas na Área do CML]()
[^4]: [Alerta de Segurança 002-2021/COTER](https://portaldopreparo.eb.mil.br/npp//images/alertas/Alerta_Seguranca_002_2021.pdf)
[^5]: [Diretriz do Comandante da 4ª Região Militar - 4ª RM]()
[^6]: [Caderno de Instrução de Prevenção de Acidentes e Gerenciamento de Risco nas Atividades Militares (EB70-CI-11.423), 1ªEdição, 2019]()